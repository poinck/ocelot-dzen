#!/bin/bash

. /etc/portage/make.conf

PCFLAGS="${CFLAGS}"

# option 5 from config.mk
PCFLAGS="${PCFLAGS} -Wall -Os -DDZEN_XFT $( pkg-config --cflags xft )"

echo "PCFLAGS = ${PCFLAGS}"

make VERSION=0 CFLAGS="${PCFLAGS}" clean install
